// 
// SE 3314b - Networking Applications
//
// Jeff Genovy (ID: 250364742)
// 
// Helper Module for creating RTP packets
//

module.exports = RTPpacket;

//
// This function creates a RTP packet with the specified payload, sequence number, and timestamp
//
function RTPpacket (payload, seq, time){

	// Generate random number for the packet sequence number if not given.
	var sequence = (typeof seq === "undefined") ? Math.floor(1000 * Math.random()) : seq;

	// Generate random number for the packet timestamp if not given.
	var timestamp = (typeof time === "undefined") ? Math.floor(1000 * Math.random()) : time;

	var packet = new Buffer(12 + payload.length);

	// We could use this, but since these fields don't change we can just use 0x80
	// this._packet[0] = (Version << 6 | Padding << 5 | eXtension << 4 | CC);
	packet[0] = 0x80;
	packet[1] = 0x1A;  // Set payload type field (PT). In this assignment we use mjpeg and the type is 26.
	packet.writeUInt16BE(sequence, 2);
	packet.writeUInt32BE(timestamp, 4);
	packet[8] = 0;
	packet[9] = 0;
	packet[10] = 0;
	packet[11] = 0;

	payload.copy(packet, 12, 0); // append payload data

	return packet;
};
