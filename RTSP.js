//
// SE 3314b - Networking Applications
//
// Jeff Genovy (ID: 250364742)
//

var util = require('util'),
	net = require('net'),
	S = require('string'),
	url = require('url'),
	RTP = require('./RTP.js');

//
// The Client object is used to keep track of the state of the client.
//
function Client(sock){
	this.id = null;				// Client ID number
	this.sock = sock;			// Client's socket
	this.rtp = null;			// RTP object
	this.intervalId = null;		// Client's Timer ID
	this.seq = null;			// Client's Sequence Number
	this.session = null;		// Client's Session Number
	/*
		This state variable is used to keep track of the clients state.
			0 - Connected
			1 - Setup command issued
			2 - Play command issued
	*/
	this.state = null;			// The current state of the Client
};

//
// This function parses the commands from the client
//
//  The argument c is the client object.
//  The data object is the data from the socket.
//
var handleClientCommand = function(c, data)
{
	var lines = String(data).split("\n");   // convert to a string
	var firstline = lines[0].split(' ');
	var command = S(firstline[0].toLowerCase()).trim().s;

	if (lines.length < 3) {
		console.log('(Client ' + c.id + ') ERROR: Invalid RTSP command. [lines.length = '+ lines.length +']');
		return;
	}

	// Save the Client Sequence Number
	var secondline = lines[1].split(':');
	var seq = S(secondline[1]).trim().toInt();

	if ((c.seq + 1) !== seq) {
		console.log('(Client ' + c.id + ') ERROR: Invalid client sequence number.');
	}
	c.seq = seq;

	if (c.state > 0)
	{
		var thirdline = lines[2].split(':');
		var session = S(thirdline[1]).trim().toInt();
		if (c.session !== session) {
			console.log('(Client ' + c.id + ') ERROR: Client Session Number does not match.');  return;
		}
	}


	if (command === "setup") {
		console.log("SETUP command.");

		if (c.state > 0) {
			console.log('(Client ' + c.id + ') ERROR: Setup command has already been issued.');  return;
		}
		
		var fileURL = url.parse( S(firstline[1]).trim().s ); // Store the File URL
		var fileParts = fileURL.pathname.split('/');
		var fileName = fileParts[fileParts.length - 1];

		var thirdline = lines[2].split(';');
		temp = S(thirdline[1].toLowerCase()).trim().s.split('=');
		var port = S(temp[1]).trim().toInt();		// Store the Client Port Number

		c.session = Math.floor(100000 * Math.random());  // Generate a random session number
		c.state = 1;  // Setup command has been issued.
		c.rtp = new RTP(port, fileName); // Create a new RTP object.

		c.sock.write("RTSP/1.0 200 OK\nCseq: " + c.seq + "\nSession: " + c.session);
		return;
	}
	if (command === "play"){
		console.log("PLAY command.");

		c.state = 2;
		c.sock.write("RTSP/1.0 200 OK\nCseq: " + c.seq + "\nSession: " + c.session);
		c.intervalId = setInterval(c.rtp.sendData, 100, c); // start the timer

		return;
	}
	if (command === "pause"){
		console.log("PAUSE command.");

		c.sock.write("RTSP/1.0 200 OK\nCseq: " + c.seq + "\nSession: " + c.session);
		clearInterval(c.intervalId); // stop the timer

		return;
	}
	if (command === "teardown"){
		console.log("TEARDOWN command.");

		if (c.state < 1) {
			console.log('(Client ' + c.id + ') ERROR: Nothing to TEARDOWN, as no PLAY command has been issued.');  return;
		}

		c.sock.write("RTSP/1.0 200 OK\nCseq: " + c.seq + "\nSession: " + c.session);
		
		clearInterval(c.intervalId); // stop the timer
		c.rtp.teardown(c);
		c.state = 0;

		return;
	}

};

// This function handles a Client exit/disconnect
//
// The argument c is the client object
//
var handleClientDisconnect = function(c){
	if (c.intervalId){
		clearInterval(c.intervalId);  // stop the timer
	}

	if (c.rtp) {
		c.rtp.teardown(c);
	}
};

var rtsp = {

	// This function handles a Client connection.
	//
	// The argument sock is the client's socket
	//
	handleClientJoin: function (sock)
	{
		var c = new Client(sock);
		c.id = Math.floor(1000 * Math.random()); // This is not a good idea as there may be duplicate ids...
		c.state = 0;

		console.log('Client '+ c.id +' Connected from: ' + sock.remoteAddress + ':' + sock.remotePort);
		sock.setEncoding();  // Make sure the socket is setup for reading.

		sock.on('data', function (data) {
			console.log('Client '+ c.id +' data:' + data);
			handleClientCommand(c, data);
		});

		sock.on('close', function (data) {
			console.log('Client '+ c.id +' Disconnected.');
			handleClientDisconnect(c);
		});
	}
};

module.exports = rtsp;
