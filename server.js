//
// SE 3314b - Networking Applications
//
// Jeff Genovy (ID: 250364742)
//

var rtsp = require('./RTSP.js'),
	net = require('net'),
	HOST = '127.0.0.1',
	PORT = 3000;

var s = net.createServer();
s.listen(PORT, HOST);

console.log('Created Server, listening on ' + HOST + ':' + PORT);

s.on('connection', function(sock){
	rtsp.handleClientJoin(sock);
});

