//
// SE 3314b - Networking Applications
//
// Jeff Genovy (ID: 250364742)
//
//
// use the RTPpacket.js and dgram 

module.exports = RTP;

var fs = require('fs'),
	net = require('net'),
	dgram = require('dgram'),
	MJPEG = require('./MJPEG.js'),
	RtpPacket = require('./RTPpacket.js');

// This is the constructor for the RTP object.
//
//  The argument client_port is the client specified port to stream to.
//  The argument requested_file is the file to stream.
//
function RTP (client_port, requested_file){
	this.port = client_port;				    	// The port to send to
	this.sock = dgram.createSocket('udp4');	     	// The socket to use
	this.seq = 1;								    // Initial Sequence Number
	this.time = Math.floor(1000 * Math.random());   // Initial Timestamp
	this.mjpeg = new MJPEG(requested_file);		    // The MJPEG object
};

// This function cleans up when the client requests to TEARDOWN
//
// The argument c is the client object
//
RTP.prototype.teardown = function(c) {
	try {
		c.rtp.mjpeg.close();
		if (c.rtp.sock) {
			c.rtp.sock.close(); // close the socket
		}
	} catch (e) {
		// do nothing
	}
};

//
// The argument c is the client object
//
RTP.prototype.sendData = function(c) {

	var data = c.rtp.mjpeg.readFrame();
	
	if (data === null) {
		clearInterval(c.intervalId);
		return;
	}

	var	packet = RtpPacket(data, c.rtp.seq, c.rtp.time);
	c.rtp.time += data.length;
	c.rtp.seq++;
	c.rtp.sock.send(packet, 0, packet.length, c.rtp.port, c.sock.remoteAddress);
};
