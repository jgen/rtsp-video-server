//
// Jeff Genovy (ID: 250364742)
//
// For reading the mjpeg files from disk. (using the fs module)

var fs = require('fs'),
	S = require('string');

module.exports = MJPEG;

// This is the constructor for the MJPEG object
//
// The argument filename is the file to open.
//
function MJPEG (filename) {
	this.file = filename;
	this.fd = fs.openSync(filename, 'r');
};

//
// This function reads a frame from the video file and returns a buffer object
// that contains that frame.
//
// (Read first 5 bytes into X, then read X bytes from the file.)
//
MJPEG.prototype.readFrame = function() {

	// First read the 5 byte header
	var bytesRead, header = new Buffer(5);
	if ((bytesRead = fs.readSync(this.fd, header, 0, header.length)) <= 0){
		fs.closeSync(this.fd);
		return null;
	}
	var frameSize = Number(header.toString());

	// Then read the video frame.
	var buf = new Buffer(frameSize);

	if ((bytesRead = fs.readSync(this.fd, buf, 0, buf.length)) <= 0){
		fs.closeSync(this.fd);
		return null;	
	}

	return buf;
};

// This function closes the file
//
MJPEG.prototype.close = function() {
	try {
		fs.close(this.fd);
	} catch (e) {
		// do nothing
	}
};
